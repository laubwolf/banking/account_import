#!/bin/sh
set -x

THIS_YEAR=$1
NEXT_YEAR=$2

# define import file names and location
CSV_LOCATION=./export/${THIS_YEAR}
CSV_KTO_CC=490473XXXXXX6011_${THIS_YEAR}-01-01_${NEXT_YEAR}-01-01.csv
CSV_KTO_GESCHAEFT=AT332032032100036544_${THIS_YEAR}-01-01_${NEXT_YEAR}-01-01.csv
CSV_KTO_PRIVAT=AT632032032102152059_${THIS_YEAR}-01-01_${NEXT_YEAR}-01-01.csv

# convert to UTF-8 and eliminate ASCII 0 characters
iconv -f UTF-16LE -t UTF-8 ${CSV_LOCATION}/${CSV_KTO_CC} | tr -d "\000" > kto-cc-${THIS_YEAR}_${CSV_KTO_CC}
iconv -f UTF-16LE -t UTF-8 ${CSV_LOCATION}/${CSV_KTO_GESCHAEFT} | tr -d "\000" > kto-geschaeft-${THIS_YEAR}_${CSV_KTO_GESCHAEFT}
iconv -f UTF-16LE -t UTF-8 ${CSV_LOCATION}/${CSV_KTO_PRIVAT} | tr -d "\000" > kto-privat-${THIS_YEAR}_${CSV_KTO_PRIVAT}

# use the following if you have edited the CSV files with LibreOffice
#iconv -t UTF-8 ${CSV_LOCATION}/${CSV_KTO_CC} | tr -d "\000" > kto-cc-${THIS_YEAR}_${CSV_KTO_CC}
#iconv -t UTF-8 ${CSV_LOCATION}/${CSV_KTO_GESCHAEFT} | tr -d "\000" > kto-geschaeft-${THIS_YEAR}_${CSV_KTO_GESCHAEFT}
#iconv -t UTF-8 ${CSV_LOCATION}/${CSV_KTO_PRIVAT} | tr -d "\000" > kto-privat-${THIS_YEAR}_${CSV_KTO_PRIVAT}

# strip off first line
find . -name kto-cc-${THIS_YEAR}_${CSV_KTO_CC} -exec sed -i "1d" {} \;
find . -name kto-geschaeft-${THIS_YEAR}_${CSV_KTO_GESCHAEFT} -exec sed -i "1d" {} \;
find . -name kto-privat-${THIS_YEAR}_${CSV_KTO_PRIVAT} -exec sed -i "1d" {} \;

# create MySQL / Google Cloud SQL tables, add categorization column and auto-guess categories 
mysql -u root -h 127.0.0.1 -pm0rket0y < kto_cc_2018_create.sql
mysql -u root -h 127.0.0.1 -pm0rket0y < kto_cc_2018_update_category.sql
mysql -u root -h 127.0.0.1 -pm0rket0y < kto_geschaeft_2018_create.sql
mysql -u root -h 127.0.0.1 -pm0rket0y < kto_geschaeft_2018_update_category.sql
mysql -u root -h 127.0.0.1 -pm0rket0y < kto_privat_2018_create.sql
#mysql -u root -h 127.0.0.1 -pm0rket0y < kto_privat_2018_update_category.sql

# make a clean table
./clean_year.sh ${THIS_YEAR}
bq rm banking_${THIS_YEAR}

# create from scratch
bq mk banking_${THIS_YEAR}

# For completeness: also create the kto_* tables in BigQuery. They have become obsolete through the use of federated MySQL instance

# define schema for temporary import
TMP_SCHEMA="_Buchungsdatum_:string,Partnername:string,Partner_IBAN:string,Partner_BIC:string,Partner_Kontonummer:integer,Partner_Bank_Code__BLZ_:integer,Betrag:string,W__hrung:string,Buchungstext:string,Ersterfassungsreferenz:string,Notiz:string,Favorit:string,Valutadatum:string,Virtuelle_Kartennummer:string,Bezahlt_mit:string,App:string"
#TMP_SCHEMA="_Buchungsdatum_:string,Partnername:string,Partner_IBAN:string,Partner_BIC:string,Partner_Kontonummer:integer,Partner_Bank_Code__BLZ_:integer,Betrag:string,W__hrung:string,Kategorie:string,Buchungstext:string,Ersterfassungsreferenz:string,Notiz:string,Favorit:string,Valutadatum:string,Virtuelle_Kartennummer:string,Bezahlt_mit:string,App:string"

# import into temporary table
bq load --replace --autodetect --field_delimiter ";" banking_${THIS_YEAR}.kto_cc kto-cc-${THIS_YEAR}_${CSV_KTO_CC} ${TMP_SCHEMA}
bq load --replace --autodetect --field_delimiter ";" banking_${THIS_YEAR}.kto_geschaeft kto-geschaeft-${THIS_YEAR}_${CSV_KTO_GESCHAEFT} ${TMP_SCHEMA}
bq load --replace --autodetect --field_delimiter ";" banking_${THIS_YEAR}.kto_privat kto-privat-${THIS_YEAR}_${CSV_KTO_PRIVAT} ${TMP_SCHEMA}

# define schema for final import
#FINAL_SCHEMA="Buchungsdatum:date,Partnername:string,Partner_IBAN:string,Partner_BIC:string,Partner_Kontonummer:integer,Partner_Bank_Code:integer,Betrag:numeric,Waehrung:string,Buchungstext:string,Ersterfassungsreferenz:string,Notiz:string,Favorit:string,Valutadatum:date,Virtuelle_Kartennummer:string,Bezahlt_mit:string,App:string"
FINAL_SCHEMA="Buchungsdatum:date,Partnername:string,Partner_IBAN:string,Partner_BIC:string,Partner_Kontonummer:integer,Partner_Bank_Code:integer,Betrag:numeric,Waehrung:string,Kategorie:string,Buchungstext:string,Ersterfassungsreferenz:string,Notiz:string,Favorit:string,Valutadatum:date,Virtuelle_Kartennummer:string,Bezahlt_mit:string,App:string"

bq mk banking_${THIS_YEAR}.konto_cc ${FINAL_SCHEMA}
bq mk banking_${THIS_YEAR}.konto_privat ${FINAL_SCHEMA}
bq mk banking_${THIS_YEAR}.konto_geschaeft ${FINAL_SCHEMA}

cat konto_cc_${THIS_YEAR}_delete.sql | bq query --use_legacy_sql=false
cat konto_cc_${THIS_YEAR}_insert_from_mycloudsql.sql | bq query --use_legacy_sql=false
cat konto_geschaeft_${THIS_YEAR}_delete.sql | bq query --use_legacy_sql=false
cat konto_geschaeft_${THIS_YEAR}_insert_from_mycloudsql.sql | bq query --use_legacy_sql=false
cat konto_privat_${THIS_YEAR}_delete.sql | bq query --use_legacy_sql=false
cat konto_privat_${THIS_YEAR}_insert_from_mycloudsql.sql | bq query --use_legacy_sql=false
