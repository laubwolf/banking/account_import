#!/bin/sh
set -x

THIS_YEAR=$1

echo "drop table banking_${THIS_YEAR}.konto_cc" | bq query --nouse_legacy_sql
echo "drop table banking_${THIS_YEAR}.kto_cc" | bq query --nouse_legacy_sql
echo "drop table banking_${THIS_YEAR}.konto_privat" | bq query --nouse_legacy_sql
echo "drop table banking_${THIS_YEAR}.kto_privat" | bq query --nouse_legacy_sql
echo "drop table banking_${THIS_YEAR}.konto_geschaeft" | bq query --nouse_legacy_sql
echo "drop table banking_${THIS_YEAR}.kto_geschaeft" | bq query --nouse_legacy_sql
