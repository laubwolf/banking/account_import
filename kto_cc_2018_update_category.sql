update banking_2018.kto_cc set Kategorie="Business.Infrastruktur.Cloud"
where
Buchungstext like "Amazon web services%" or
Buchungstext like "AWS EMEA%" or
Buchungstext like "GOOGLE *CLOUD%" or
Buchungstext like "GOOGLE *GSUITE%" or
Buchungstext like "GOOGLE *SVCSAPPS%" or
Buchungstext like "ITUNES.COM/BILL%" or
Buchungstext like "MSFT *%";

update banking_2018.kto_cc set Kategorie="Business.Mobilität.Nahverkehr"
where
Buchungstext like "BVG APP%" or
Buchungstext like "VGN TICKET%" or
Buchungstext like "www.rmv.de%";

update banking_2018.kto_cc set Kategorie="Business.Mobilität.Zug"
where
Buchungstext like "DB %" or
Buchungstext like "OBB%" or
Buchungstext like "OEBB%";

update banking_2018.kto_cc set Kategorie="Business.Unterkunft"
where
upper(Buchungstext) like "%HOTEL%" or
upper(Buchungstext) like "%AIRBNB%" or
Buchungstext like "LEONARDO%";

update banking_2018.kto_cc set Kategorie="Privat.Gesundheit.Heilbehelfe"
where
upper(Buchungstext) like "%APOTHEKE%" or
Buchungstext like "Optiker Aigner%";

update banking_2018.kto_cc set Kategorie="Privat.Haus.Garten"
where
upper(Buchungstext) like "%FLORISTIK%";

update banking_2018.kto_cc set Kategorie="Privat.Haus.Geräte"
where
upper(Buchungstext) like "%OBI %" or
upper(Buchungstext) like "%WEYLAND %";

update banking_2018.kto_cc set Kategorie="Privat.Leben.Hygiene"
where
upper(Buchungstext) like "%FRISEUR %" or
upper(Buchungstext) like "ROSSMANN %";

update banking_2018.kto_cc set Kategorie="Privat.Leben.Kleidung"
where
upper(Buchungstext) like "%CLOPPENBURG%";

update banking_2018.kto_cc set Kategorie="Privat.Leben.Nahrung"
where
upper(Buchungstext) like "BILLA %" or
upper(Buchungstext) like "DAHOAM%" or
upper(Buchungstext) like "EUROSPAR%" or
upper(Buchungstext) like "INNVIERTLER NATUR-MARKT%" or
upper(Buchungstext) like "TEEROSE%";

update banking_2018.kto_cc set Kategorie="Privat.Leben.Rauchen"
where
upper(Buchungstext) like "%TABAK%" or
upper(Buchungstext) like "%TRAFIK%";

update banking_2018.kto_cc set Kategorie="Privat.Leben.Wirtshaus"
where
upper(Buchungstext) like "%WIRT%" or
upper(Buchungstext) like "SEVEN%";

update banking_2018.kto_cc set Kategorie="Privat.Medien"
where
Buchungstext like "Amazon Digital Svcs%" or
Buchungstext like "AMZNPRIME%" or
Buchungstext like "DAZN.com%" or
Buchungstext like "%EUROSPORT%" or
Buchungstext like "%PLAYSTATION%" or
Buchungstext like "Prime Video%";
