update banking_2018.konto_geschaeft set Kategorie="Business.Bank.Entnahme"
where
Buchungstext like "SB-Auszahlung %" or
Buchungstext like "George-%Andj%" or
Buchungstext like "Barauszahlung%";

update banking_2018.konto_geschaeft set Kategorie="Business.Bank.Spesen"
where
Buchungstext like "Überziehungsprovison%" or
Buchungstext like "Sollzinsen%" or
Buchungstext like "Rahmenbereitstellung%" or
Buchungstext like "Plug-In Entgelt%" or
Buchungstext like "Kostenersatz%" or
Buchungstext like "Kontoführung%" or
Buchungstext like "Kartenservice%";

update banking_2018.konto_geschaeft set Kategorie="Business.Bank.Übertrag"
where
Buchungstext like "SB-Übertrag %" or
Buchungstext like "Saldoausgleich Wolfram Laube%" or
Buchungstext like "%Kreditkartenrechnung%" or
Buchungstext like "George-%Wolf%" or
Buchungstext like "Eigenübertrag%";

update banking_2018.konto_geschaeft set Kategorie="Business.Honorar"
where
upper(Buchungstext) like "% STHREE%" or
upper(Buchungstext) like "% VARIUS IT%" or
upper(Buchungstext) like "% KRONGAARD AG%" or
upper(Buchungstext) like "% SOMI SOLUTIONS%" or
upper(Buchungstext) like "% AMORIA BOND%" or
upper(Buchungstext) like "% HAYS AG%";

update banking_2018.konto_geschaeft set Kategorie="Business.Infrastruktur.Medien"
where
upper(Buchungstext) like "%DREI AUSTRIA%";

update banking_2018.konto_geschaeft set Kategorie="Business.Mobilität.Zug"
where
upper(Buchungstext) like "DB %" or
upper(Buchungstext) like "%ÖBB %" or
upper(Buchungstext) like "%OEBB %";

update banking_2018.konto_geschaeft set Kategorie="Business.Administration.Steuerberatung"
where
upper(Buchungstext) like "%J. BIRN %";

update banking_2018.konto_geschaeft set Kategorie="Business.Administration.Finanzamt"
where
upper(Buchungstext) like "%FINANZAMT%";

update banking_2018.konto_geschaeft set Kategorie="Business.Administration.Sozialversicherung.Andja"
where
Buchungstext like "5258210577%";

update banking_2018.konto_geschaeft set Kategorie="Business.Administration.Sozialversicherung.Wolf"
where
Buchungstext like "1254210969%";

update banking_2018.konto_geschaeft set Kategorie="Privat.Auto.Kredit"
where
Buchungstext like "%VKB%";

update banking_2018.konto_geschaeft set Kategorie="Privat.Auto.Instandhaltung"
where
Buchungstext like "%ÖAMTC%";

update banking_2018.konto_geschaeft set Kategorie="Privat.Auto.Strafe"
where
upper(Buchungstext) like "VERK%R96%" or
upper(Buchungstext) like "%STRAF%";

update banking_2018.konto_geschaeft set Kategorie="Privat.Auto.Versicherung"
where
upper(Buchungstext) like "%804673758%" or
upper(Buchungstext) like "%408728261%";

update banking_2018.konto_geschaeft set Kategorie="Privat.Gesundheit.Arzt"
where
upper(Buchungstext) like "%DR. %" or
upper(Buchungstext) like "%KRANKENHAUS%" or
upper(Buchungstext) like "%KLINIK%";

update banking_2018.konto_geschaeft set Kategorie="Privat.Haus.Garten"
where
upper(Buchungstext) like "%JOH. POINDECKER%";

update banking_2018.konto_geschaeft set Kategorie="Privat.Haus.Geräte"
where
upper(Buchungstext) like "%WEYLAND%";

update banking_2018.konto_geschaeft set Kategorie="Privat.Haus.Kredit.Versicherung"
where
upper(Buchungstext) like "%82779171%";

update banking_2018.konto_geschaeft set Kategorie="Privat.Haus.Kredit.Versicherung"
where
upper(Buchungstext) like "%82779171%";

update banking_2018.konto_geschaeft set Kategorie="Privat.Haus.Instandhaltung"
where
upper(Buchungstext) like "%FORKL%" or
upper(Buchungstext) like "%BRAUMANN%" or
upper(Buchungstext) like "%LÖGER MADER%" or
upper(Buchungstext) like "%ELEKTRO HAAS%";

update banking_2018.konto_geschaeft set Kategorie="Privat.Haus.Strom"
where
upper(Buchungstext) like "%ENERGIE AG%";

update banking_2018.konto_geschaeft set Kategorie="Privat.Haus.Versicherung"
where
upper(Buchungstext) like "%OBERÖSTERREICHISCHE VERS%" or
upper(Buchungstext) like "%OÖ VERS%";

update banking_2018.konto_geschaeft set Kategorie="Privat.Leben.Kirche"
where
upper(Buchungstext) like "%KATHOLISCHE KIRCHE%";

update banking_2018.konto_geschaeft set Kategorie="Privat.Leben.Schule"
where
upper(Buchungstext) like "%LSR%OBER%";

update banking_2018.konto_geschaeft set Kategorie="Privat.Leben.Urlaub"
where
upper(Buchungstext) like "%TICKETS 75%" or
upper(Buchungstext) like "%JESOLO%";

update banking_2018.konto_geschaeft set Kategorie="Privat.Medien"
where
upper(Buchungstext) like "%GIS GEBÜHREN%";

