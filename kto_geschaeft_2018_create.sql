/*
# Start Cloud SQL Proxy and authenticate using service account
docker run -d   -v /home/wolf/Downloads/myk8sproject-207017-95ba647c5528-cloudsql-admin.json:/config   -p 127.0.0.1:3306:3306   gcr.io/cloudsql-docker/gce-proxy:1.12 /cloud_sql_proxy   -instances=myk8sproject-207017:us-central1:mycloudsql=tcp:0.0.0.0:3306 -credential_file=/config

# Connect over proxy
mysql -u root -h 127.0.0.1 -p
*/

/* Create and use schema */
CREATE SCHEMA IF NOT EXISTS banking_2018;
USE banking_2018;

/* Create table for temporarily storing CSV data */
DROP TABLE IF EXISTS kto_geschaeft;
CREATE TABLE kto_geschaeft (
_Buchungsdatum_ VARCHAR(255),
Partnername VARCHAR(255),
Partner_IBAN VARCHAR(255),
Partner_BIC VARCHAR(255),
Partner_Kontonummer INT,
Partner_Bank_Code__BLZ_ INT,
Betrag VARCHAR(255),
W__hrung VARCHAR(255),
Buchungstext VARCHAR(255),
Ersterfassungsreferenz VARCHAR(255),
Notiz VARCHAR(255),
Favorit VARCHAR(255),
Valutadatum VARCHAR(255),
Virtuelle_Kartennummer VARCHAR(255),
Bezahlt_mit VARCHAR(255),
App VARCHAR(255)
);

/* Upload exported banking data CSV file */
LOAD DATA LOCAL INFILE 'kto-geschaeft-2018_AT332032032100036544_2018-01-01_2019-01-01.csv'
INTO TABLE banking_2018.kto_geschaeft
FIELDS TERMINATED BY ';'
ENCLOSED BY '"'
LINES TERMINATED BY '\n'/*
IGNORE 1 ROWS*/;

ALTER TABLE kto_geschaeft ADD Kategorie VARCHAR(255) AFTER W__hrung; 
