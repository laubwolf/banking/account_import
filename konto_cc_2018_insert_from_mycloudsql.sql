insert into banking_2018.konto_cc(
Buchungsdatum,
Partnername,
Partner_IBAN,
Partner_BIC,
Partner_Kontonummer,
Partner_Bank_Code,
Betrag,
Waehrung,
Kategorie,
Buchungstext,
Ersterfassungsreferenz,
Notiz,
Favorit,
Valutadatum,
Virtuelle_Kartennummer,
Bezahlt_mit,
App
)
SELECT * FROM EXTERNAL_QUERY(
'myk8sproject-207017.us.mycloudsql-banking_2018',
'select STR_TO_DATE(_Buchungsdatum_, "%d.%m.%Y") as _Buchungsdatum_, Partnername, Partner_IBAN, Partner_BIC, Partner_Kontonummer, Partner_Bank_Code__BLZ_, cast(replace(Betrag,",",".") as decimal(10,2)) as Betrag, W__hrung, Kategorie, Buchungstext, Ersterfassungsreferenz, Notiz, concat(Favorit, "") as Favorit, str_to_date(Valutadatum, "%d.%m.%Y") as Valutadatum, Virtuelle_Kartennummer, Bezahlt_mit, App from banking_2018.kto_cc');
